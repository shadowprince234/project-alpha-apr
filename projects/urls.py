from django.urls import path
from .views import show_project, project_detail, create_project


urlpatterns = [
    path("", show_project, name="list_projects"),
    path("<int:id>/", project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
